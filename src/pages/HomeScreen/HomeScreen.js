import React, {useEffect} from 'react';
import {View, SafeAreaView} from 'react-native';
import MyButton from '../../components/MyButton/MyButton';
import MyText from '../../components/Text/MyText';
import {openDatabase} from 'react-native-sqlite-storage';
import styles from './home-screen.css';

const db = openDatabase({name: 'UserDatabase.db'});

const HomeScreen = ({navigation}) => {
  useEffect(() => {
    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='user'",
        [],
        function (tx, res) {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS user', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
              [],
            );
          }
        },
      );
    });
  }, []);

  return (
    <SafeAreaView style={styles.homeScreenSafeAreaView}>
      <View style={styles.homeScreenViewPage}>
        <View style={styles.homeScreenSafeAreaView}>
          <MyText text="SQLite Database" />

          <View style={styles.buttonView}>
            <MyButton
              title="Add New User"
              onPress={() => navigation.navigate('AddUser')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="Update User"
              onPress={() => navigation.navigate('UpdateUser')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="View User"
              onPress={() => navigation.navigate('ViewUser')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="View All User"
              onPress={() => navigation.navigate('ViewAllUser')}
            />
          </View>
          <View style={styles.buttonView}>
            <MyButton
              title="View Users Between Range"
              onPress={() => navigation.navigate('ViewUsersRange')}
            />
          </View>

          <View style={styles.buttonView}>
            <MyButton
              title="Delete User"
              onPress={() => navigation.navigate('DeleteUser')}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
