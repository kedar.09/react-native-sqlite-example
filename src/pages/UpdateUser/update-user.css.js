import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  updateUserSafeAreaView: {
    flex: 1,
  },
  updateUserViewPage: {
    flex: 1,
    backgroundColor: 'white',
  },
  updateUserKeyboardAvoidView: {
    flex: 1,
    justifyContent: 'space-between',
  },
  updateUserTextInput: {
    padding: 10,
  },
  updateUserTextInputAddress: {
    textAlignVertical: 'top',
    padding: 10,
  },
  buttonView: {
    margin: 20,
  },
  inputErrorStyle: {
    color: 'red',
  },
});
