import React, {useState} from 'react';
import {View, Alert, SafeAreaView} from 'react-native';
import MyTextInput from '../../components/MyTextInput/MyTextInput';
import MyButton from '../../components/MyButton/MyButton';
import {openDatabase} from 'react-native-sqlite-storage';
import styles from './view-user.css';
import * as yup from 'yup';
import {Formik} from 'formik';
import {Text} from 'react-native-elements';

const db = openDatabase({name: 'UserDatabase.db'});

const ViewUser = () => {
  const [userData, setUserData] = useState({});

  return (
    <SafeAreaView style={styles.viewUserSafeAreaView}>
      <View style={styles.viewUserViewPage}>
        <View style={styles.viewUserSafeAreaView}>
          <Formik
            enableReinitialize={true}
            initialValues={{inputUserId: ''}}
            onSubmit={(values) => {
              console.log(values.inputUserId);
              setUserData({});
              db.transaction((tx) => {
                tx.executeSql(
                  'SELECT * FROM user where user_id = ?',
                  [values.inputUserId],
                  (tx, results) => {
                    var len = results.rows.length;
                    console.log('len', len);
                    if (len > 0) {
                      setUserData(results.rows.item(0));
                    } else {
                      Alert.alert('No user found');
                    }
                  },
                );
              });
            }}
            validationSchema={yup.object().shape({
              inputUserId: yup
                .number()
                .typeError("Doesn't seems like User ID")
                .positive("User ID can't start with a minus")
                .integer("User ID can't include decimal number")
                .min(1, 'Enter valid User ID')
                .max(9999999999, 'Enter valid User ID')
                .required('User ID is required'),
            })}>
            {(props) => (
              <View>
                <MyTextInput
                  errorMessage={
                    props.errors.inputUserId && props.touched.inputUserId
                      ? props.errors.inputUserId
                      : null
                  }
                  value={props.values.inputUserId}
                  onBlur={props.handleBlur('inputUserId')}
                  errorStyle={styles.inputErrorStyle}
                  onChangeText={props.handleChange('inputUserId')}
                  placeholder="Enter User Id"
                  style={styles.viewUserTextInput}
                />
                <View style={styles.buttonView}>
                  <MyButton title="Search User" onPress={props.handleSubmit} />
                </View>
              </View>
            )}
          </Formik>
          <View style={styles.viewUserData}>
            <Text>User Id: {userData.user_id}</Text>
            <Text>User Name: {userData.user_name}</Text>
            <Text>User Contact: {userData.user_contact}</Text>
            <Text>User Address: {userData.user_address}</Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ViewUser;
