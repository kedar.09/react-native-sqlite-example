import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  addUserSafeAreaView: {
    flex: 1,
  },
  addUserViewPage: {
    flex: 1,
    backgroundColor: 'white',
  },
  addUserKeyboardAvoidView: {
    flex: 1,
    justifyContent: 'space-between',
  },
  addUserTextInput: {
    padding: 10,
  },
  addUserTextInputAddress: {
    textAlignVertical: 'top',
    padding: 10,
  },
  buttonView: {
    margin: 20,
  },
  inputErrorStyle: {
    color: 'red',
  },
});
