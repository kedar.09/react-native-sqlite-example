import React, {useState, useEffect} from 'react';
import {FlatList, View, SafeAreaView} from 'react-native';
import {openDatabase} from 'react-native-sqlite-storage';
import styles from './view-all-user.css';
import {Text} from 'react-native-elements';

const db = openDatabase({name: 'UserDatabase.db'});

const ViewAllUser = () => {
  let [flatListItems, setFlatListItems] = useState([]);

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql('SELECT * FROM user', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i)
          temp.push(results.rows.item(i));
        setFlatListItems(temp);
      });
    });
  }, []);

  let listViewItemSeparator = () => {
    return <View style={styles.viewAllUserListViewItemSeparator} />;
  };

  let listItemView = (item) => {
    return (
      <View key={item.user_id} style={styles.viewAllUserListViewItem}>
        <Text>Id: {item.user_id}</Text>
        <Text>Name: {item.user_name}</Text>
        <Text>Contact: {item.user_contact}</Text>
        <Text>Address: {item.user_address}</Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.viewAllUserSafeAreaView}>
      <View style={styles.viewAllUserViewPage}>
        <View style={styles.viewAllUserSafeAreaView}>
          <FlatList
            data={flatListItems}
            ItemSeparatorComponent={listViewItemSeparator}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => listItemView(item)}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ViewAllUser;
