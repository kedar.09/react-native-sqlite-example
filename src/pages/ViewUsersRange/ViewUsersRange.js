import React, {useState, useEffect} from 'react';
import {FlatList, View, Alert, SafeAreaView} from 'react-native';
import {openDatabase} from 'react-native-sqlite-storage';
import styles from './view-users-range.css';
import {Text} from 'react-native-elements';
import MyButton from '../../components/MyButton/MyButton';
import MyTextInput from '../../components/MyTextInput/MyTextInput';
import {Formik} from 'formik';
import * as yup from 'yup';

const db = openDatabase({name: 'UserDatabase.db'});

const ViewUsersRange = () => {
  let [flatListItems, setFlatListItems] = useState([]);

  let listViewItemSeparator = () => {
    return <View style={styles.viewAllUserListViewItemSeparator} />;
  };

  let listItemView = (item) => {
    return (
      <View key={item.user_id} style={styles.viewAllUserListViewItem}>
        <Text>Id: {item.user_id}</Text>
        <Text>Name: {item.user_name}</Text>
        <Text>Contact: {item.user_contact}</Text>
        <Text>Address: {item.user_address}</Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.viewAllUserSafeAreaView}>
      <View style={styles.viewAllUserViewPage}>
        <Formik
          enableReinitialize={true}
          initialValues={{inputUserId: ''}}
          onSubmit={(values) => {
            console.log(values.inputUserId);
            setFlatListItems([]);
            db.transaction((tx) => {
              tx.executeSql(
                'SELECT * FROM user where user_id >= ? and user_id <= ?',
                [values.fromUserId, values.toUserId],
                (tx, results) => {
                  var len = results.rows.length;
                  console.log('len', len);
                  var temp = [];
                  for (let i = 0; i < results.rows.length; ++i)
                    temp.push(results.rows.item(i));
                  setFlatListItems(temp);
                },
              );
            });
          }}
          validationSchema={yup.object().shape({
            fromUserId: yup
              .number()
              .typeError("Doesn't seems like User ID")
              .positive("User ID can't start with a minus")
              .integer("User ID can't include decimal number")
              .min(1, 'Enter valid User ID')
              .max(9999999999, 'Enter valid User ID')
              .required('User ID is required'),
            toUserId: yup
              .number()
              .typeError("Doesn't seems like User ID")
              .positive("User ID can't start with a minus")
              .integer("User ID can't include decimal number")
              .min(1, 'Enter valid User ID')
              .max(9999999999, 'Enter valid User ID')
              .required('User ID is required'),
          })}>
          {(props) => (
            <View>
              <MyTextInput
                errorMessage={
                  props.errors.fromUserId && props.touched.fromUserId
                    ? props.errors.fromUserId
                    : null
                }
                value={props.values.fromUserId}
                onBlur={props.handleBlur('fromUserId')}
                errorStyle={styles.inputErrorStyle}
                onChangeText={props.handleChange('fromUserId')}
                placeholder="Enter From User Id"
                style={styles.viewUserTextInput}
              />
              <MyTextInput
                errorMessage={
                  props.errors.toUserId && props.touched.toUserId
                    ? props.errors.toUserId
                    : null
                }
                value={props.values.toUserId}
                onBlur={props.handleBlur('toUserId')}
                errorStyle={styles.inputErrorStyle}
                onChangeText={props.handleChange('toUserId')}
                placeholder="Enter To User Id"
                style={styles.viewUserTextInput}
              />
              <View style={styles.buttonView}>
                <MyButton title="Search User" onPress={props.handleSubmit} />
              </View>
            </View>
          )}
        </Formik>

        <View style={styles.viewAllUserSafeAreaView}>
          <FlatList
            data={flatListItems}
            ItemSeparatorComponent={listViewItemSeparator}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => listItemView(item)}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ViewUsersRange;
