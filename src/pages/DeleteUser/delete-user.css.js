import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  deleteUserSafeAreaView: {
    flex: 1,
  },
  deleteUserViewPage: {
    flex: 1,
    backgroundColor: 'white',
  },
  deleteUserTextInput: {
    padding: 10,
  },
  buttonView: {
    margin: 20,
  },
  inputErrorStyle: {
    color: 'red',
  },
});
