import React, {useState} from 'react';
import {View, Alert, SafeAreaView} from 'react-native';
import MyTextInput from '../../components/MyTextInput/MyTextInput';
import MyButton from '../../components/MyButton/MyButton';
import {openDatabase} from 'react-native-sqlite-storage';
import styles from './delete-user.css';
import * as yup from 'yup';
import {Formik} from 'formik';

const db = openDatabase({name: 'UserDatabase.db'});

const DeleteUser = ({navigation}) => {
  return (
    <SafeAreaView style={styles.deleteUserSafeAreaView}>
      <View style={styles.deleteUserViewPage}>
        <View style={styles.deleteUserSafeAreaView}>
          <Formik
            enableReinitialize={true}
            initialValues={{inputUserId: ''}}
            onSubmit={(values) => {
              db.transaction((tx) => {
                tx.executeSql(
                  'DELETE FROM  user where user_id=?',
                  [values.inputUserId],
                  (tx, results) => {
                    console.log('Results', results.rowsAffected);
                    if (results.rowsAffected > 0) {
                      Alert.alert(
                        'Success',
                        'User deleted successfully',
                        [
                          {
                            text: 'Ok',
                            onPress: () => navigation.navigate('HomeScreen'),
                          },
                        ],
                        {cancelable: false},
                      );
                    } else {
                      Alert.alert('User not found');
                    }
                  },
                );
              });
            }}
            validationSchema={yup.object().shape({
              inputUserId: yup
                .number()
                .typeError("Doesn't seems like User ID")
                .positive("User ID can't start with a minus")
                .integer("User ID can't include decimal number")
                .min(1, 'Enter valid User ID')
                .max(9999999999, 'Enter valid User ID')
                .required('User ID is required'),
            })}>
            {(props) => (
              <View>
                <MyTextInput
                  errorMessage={
                    props.errors.inputUserId && props.touched.inputUserId
                      ? props.errors.inputUserId
                      : null
                  }
                  value={props.values.inputUserId}
                  onBlur={props.handleBlur('inputUserId')}
                  errorStyle={styles.inputErrorStyle}
                  onChangeText={props.handleChange('inputUserId')}
                  placeholder="Enter User Id"
                  style={styles.viewUserTextInput}
                />
                <View style={styles.buttonView}>
                  <MyButton title="Delete User" onPress={props.handleSubmit} />
                </View>
              </View>
            )}
          </Formik>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default DeleteUser;
